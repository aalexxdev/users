const {User} = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Joi = require('joi');
const auth = require('../middleware/auth');
router.get('/', async (req, res) => {
  const votes = await User.find({}).select('name votes').sort({votes: -1}).limit(10);
  res.send(votes);
});

router.put('/:id', auth, async (req, res) => {
  const vote = await User.findByIdAndUpdate(req.params.id, { $inc: { votes: 1 } });
  if (!vote) return res.status(404).send('User not found.');
  res.status(200).send('OK');
});


module.exports = router;