# Users  

Sign in / Login App with FullRest API BackEnd.   
 
- Built with React     
- Sign in / Login.  
- Json Web Token.
- FullRest Backend in Node and MongoDB. 
- BCryptJs. 
- Vote system for users logged in.
- Shows in a D3 Graph top 10 users based on their votings
- Responsive 
- State management with Redux
- Beautify code on git commit
- Redux state logger on dev
- Persistence, keeps storage / state on window reload or open / close browser, till clear browser data.

  
### Installation

Install the repo and launch it.

```sh
$ git clone https://aalexx1978@bitbucket.org/aalexx1978/users.git 
$ cd users
$ yarn install
$ yarn start  
```   
   
_on Mac_ to start in other port just run   

```PORT=3001 yarn start```  
  
##  Screenshots
#### Main screen  
![one](./assets/demo.gif)  
#### Responsive  
![two](./assets/resp.png)  
#### MLab Mongo DB  
![three](./assets/mlab.png)  
#### Apitesting with Postman  
![three](./assets/postman.png)  
#### redux-logger with votes  
![one](./assets/votes_logger.png)  
#### API Call for voting  
![two](./assets/apivote.png)  

