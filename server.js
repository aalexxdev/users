const config = require("config");
const Joi = require("joi");
const mongoose = require("mongoose");
const users = require("./routes/users");
const votes = require("./routes/votes");
const auth = require("./routes/auth");
const express = require("express");
const chalk = require("chalk");
const log = console.log;
Joi.objectId = require("joi-objectid")(Joi);

const app = express();
app.get("/", (req, res) => {
    res.send("Ok");
});
app.use(express.json());
app.use("/api/users", users);
app.use("/api/auth", auth);
app.use("/api/votes", votes);
if (!config.get("jwtPrivateKey")) {
    console.error("FATAL ERROR: jwtPrivateKey is not defined.");
    process.exit(1);
}

mongoose
    .connect("mongodb://user:user1234@ds115543.mlab.com:15543/users", {
        useUnifiedTopology: true,
        useCreateIndex: true,
        useNewUrlParser: true
    })
    .then(() => log(chalk.white.bgBlue.bold("Connected to MongoDB...")))
    .catch(err => console.dir(err));

const port = 3002;
app.listen(port, () => log(chalk.bgGreen.bold(`Listening on port ${port}...`)));
