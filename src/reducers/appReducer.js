import initialState from "./initialState";
import {
    FETCH_VOTES,
    RECEIVE_VOTES,
    SET_ERROR,
    SET_SIZE,
    SET_SHOW,
    SET_TOKEN,
    SET_USER,
    USER_REGISTER,
    LOGOUT
} from "../actions/allActions";

export default function appRed(state = initialState, action) {
    switch (action.type) {
        case FETCH_VOTES:
            return action;
        case SET_TOKEN:
            return {
                ...state,
                token: action.token,
                register: null,
                error: null
            };
        case RECEIVE_VOTES:
            return { ...state, votes: action.votes, error: null };
        case SET_ERROR:
            return { ...state, error: action.error };
        case SET_SIZE:
            return { ...state, width: action.width };
        case SET_SHOW:
            return { ...state, show: action.show };
        case SET_USER:
            return { ...state, user: action.user, error: null };
        case LOGOUT:
            return initialState;
        case USER_REGISTER:
            return { ...state, register: action.register, error: null };
        default:
            return state;
    }
}
