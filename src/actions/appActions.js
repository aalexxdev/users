import {
    RECEIVE_VOTES,
    SET_ERROR,
    SET_SIZE,
    SET_SHOW,
    SET_TOKEN,
    SET_USER,
    USER_REGISTER,
    LOGOUT
} from "./allActions";
import axios from "axios";

export function receiveVotes(votes) {
    return { type: RECEIVE_VOTES, votes: votes };
}
export function updateSize(width) {
    return { type: SET_SIZE, width: width };
}

export function setError(error) {
    return { type: SET_ERROR, error: error };
}
export function setShow(val) {
    return { type: SET_SHOW, show: val };
}
export function receiveLogin(token) {
    return { type: SET_TOKEN, token: token };
}
export function userRegister(info) {
    return { type: USER_REGISTER, register: info };
}

export function loginOut() {
    return { type: LOGOUT };
}
export function logOut() {
    return dispatch => {
        dispatch(loginOut());
        dispatch(fetchVotes());
    };
}
export function setUser(user) {
    return { type: SET_USER, user: user };
}
export function fetchVotes() {
    return dispatch => {
        axios
            .get("/votes")
            .then(function(votes) {
                dispatch(receiveVotes(votes.data.sort()));
            })
            .catch(function(error) {
                dispatch(setError(error.message));
            });
    };
}
export function login(email, password) {
    const postData = {
        email,
        password
    };
    const header = {
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
        }
    };
    return dispatch => {
        axios
            .post("/auth", postData, header)
            .then(function(response) {
                dispatch(receiveLogin(response.data));
                dispatch(getUser(response.data));
            })
            .catch(function(error) {
                dispatch(setError(error.message));
            });
    };
}

export function getUser(token) {
    const headers = {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            "x-auth-token": token
        }
    };
    return dispatch => {
        axios
            .get("/users/me", headers)
            .then(function(response) {
                dispatch(setUser(response.data));
            })
            .catch(function(error) {
                dispatch(setError(error.message));
            });
    };
}
export function vote(token, id) {
    const headrs = {
        headers: {
            "Content-Type": "application/json;charset=UTF-8",
            "x-auth-token": token
        }
    };
    return dispatch => {
        axios
            .put("/votes/" + id, null, headrs)
            .then(function(response) {
                dispatch(fetchVotes());
            })
            .catch(function(error) {
                dispatch(setError(error.message));
            });
    };
}
export function register(name, email, password) {
    const headrs = {
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
        }
    };
    const datos = {
        name,
        email,
        password,
        votes: 0
    };
    return dispatch => {
        axios
            .post("/users/", datos, headrs)
            .then(function(response) {
                dispatch(userRegister(response.data));
            })
            .catch(function(error) {
                dispatch(setError(error.message));
            });
    };
}
