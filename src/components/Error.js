import React from "react";
import { Button, Fade, Alert } from "reactstrap";

export default class Error extends React.Component {
    constructor(props) {
        super(props);
        this.state = { fadeIn: true };
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.props.appActions.setError(null);
        this.setState({
            fadeIn: !this.state.fadeIn
        });
    }
    render() {
        const { error } = this.props;
        if (this.props.error !== null) {
            return (
                <div>
                    <Fade in={this.state.fadeIn} tag="h5" className="mt-3">
                        <Alert color="danger">
                            {error}{" "}
                            <Button color="danger" onClick={this.toggle}>
                                Dismiss
                            </Button>
                        </Alert>
                    </Fade>
                </div>
            );
        } else {
            return null;
        }
    }
}
