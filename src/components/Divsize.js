import React, { Component } from "react";
import sizeMe from "react-sizeme";

class Divsize extends Component {
    componentDidMount() {
        this.props.appActions.updateSize(this.props.size.width);
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.size.width !== nextProps.size.width) {
            this.props.appActions.updateSize(nextProps.size.width);
        }
    }
    render() {
        return <div> </div>;
    }
}

export default sizeMe({ monitorWidth: true })(Divsize);
