import React from "react";
import { Button, Form, FormGroup, Label, Input, Tooltip } from "reactstrap";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changePass = this.changePass.bind(this);
        this.onLogOut = this.onLogOut.bind(this);
        this.toggle = this.toggle.bind(this);
        this.state = {
            email: "",
            pass: "",
            tooltipOpen: false,
            validate: {
                emailState: "",
                passState: ""
            }
        };
    }
    validateEmail(e) {
        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const { validate } = this.state;
        if (emailRex.test(e.target.value)) {
            validate.emailState = "has-success";
        } else {
            validate.emailState = "has-danger";
        }
        this.setState({ validate });
    }
    validatePass(e) {
        const { validate } = this.state;
        if (e.target.value.length >= 4) {
            validate.passState = "has-success";
        } else {
            validate.passState = "has-danger";
        }
        this.setState({ validate });
    }
    onSubmit(e) {
        e.preventDefault();
        if (this.state.email === "" || this.state.pass === "") {
            return false;
        } else {
            this.props.appActions.login(this.state.email, this.state.pass);
        }
    }
    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }
    changeEmail(e) {
        this.setState({ email: e.target.value });
    }
    onLogOut(e) {
        e.preventDefault();
        this.props.appActions.logOut();
        localStorage.clear();
    }
    changePass(e) {
        this.setState({ pass: e.target.value });
    }
    render() {
        if (this.props.token === null) {
            let disabled = !(
                this.state.validate.passState === "has-success" &&
                this.state.validate.emailState === "has-success"
            );
            return (
                <Form onSubmit={e => this.onSubmit(e)} inline>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0 col-xs-12">
                        <Label for="theEmail" className="mr-sm-2">
                            Email
                        </Label>
                        <Input
                            value={this.state.email}
                            onChange={e => {
                                this.changeEmail(e);
                                this.validateEmail(e);
                            }}
                            valid={
                                this.state.validate.emailState === "has-success"
                            }
                            invalid={
                                this.state.validate.emailState === "has-danger"
                            }
                            type="email"
                            name="email"
                            id="theEmail"
                            placeholder="user@domain.com"
                        />
                        <Tooltip
                            placement="bottom"
                            isOpen={this.state.tooltipOpen}
                            target="theEmail"
                            toggle={this.toggle}
                        >
                            email: alex@alex.com
                        </Tooltip>
                    </FormGroup>
                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                        <Label for="thePassword" className="mr-sm-2">
                            Password
                        </Label>
                        <Input
                            value={this.state.pass}
                            onChange={e => {
                                this.changePass(e);
                                this.validatePass(e);
                            }}
                            valid={
                                this.state.validate.passState === "has-success"
                            }
                            invalid={
                                this.state.validate.passState === "has-danger"
                            }
                            type="password"
                            name="password"
                            id="thePassword"
                            placeholder="your pass"
                        />
                        <Tooltip
                            placement="bottom"
                            isOpen={this.state.tooltipOpen}
                            target="thePassword"
                            toggle={this.toggle}
                        >
                            Pass: alex
                        </Tooltip>
                    </FormGroup>
                    <Button
                        className="posFix"
                        onClick={e => this.onSubmit(e)}
                        disabled={disabled}
                    >
                        Access
                    </Button>
                </Form>
            );
        } else {
            return <Button onClick={e => this.onLogOut(e)}>Log Out</Button>;
        }
    }
}
