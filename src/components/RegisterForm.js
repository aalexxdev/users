import React from "react";
import {
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Alert,
    FormFeedback
} from "reactstrap";

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changePass = this.changePass.bind(this);
        this.state = {
            email: "",
            pass: "",
            name: "",
            validate: {
                emailState: "",
                nameState: "",
                passState: ""
            }
        };
    }
    validateEmail(e) {
        const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const { validate } = this.state;
        if (emailRex.test(e.target.value)) {
            validate.emailState = "has-success";
        } else {
            validate.emailState = "has-danger";
        }
        this.setState({ validate });
    }
    validateName(e) {
        const { validate } = this.state;
        if (e.target.value.length >= 4) {
            validate.nameState = "has-success";
        } else {
            validate.nameState = "has-danger";
        }
        this.setState({ validate });
    }
    validatePass(e) {
        const { validate } = this.state;
        if (e.target.value.length >= 4) {
            validate.passState = "has-success";
        } else {
            validate.passState = "has-danger";
        }
        this.setState({ validate });
    }
    onSubmit(e) {
        e.preventDefault();
        if (this.state.email === "" || this.state.pass === "") {
            return false;
        } else {
            this.props.appActions.register(
                this.state.name,
                this.state.email,
                this.state.pass
            );
        }
    }
    changeEmail(e) {
        this.setState({ email: e.target.value });
    }
    changeName(e) {
        this.setState({ name: e.target.value });
    }
    changePass(e) {
        this.setState({ pass: e.target.value });
    }
    render() {
        if (this.props.token === null) {
            if (this.props.register === null) {
                let disabled = !(
                    this.state.validate.passState === "has-success" &&
                    this.state.validate.nameState === "has-success" &&
                    this.state.validate.emailState === "has-success"
                );
                return (
                    <Form onSubmit={e => this.onSubmit(e)}>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0 col-xs-12">
                            <Label for="theName" className="mr-sm-2">
                                Name
                            </Label>
                            <Input
                                className="text-capitalize"
                                valid={
                                    this.state.validate.nameState ===
                                    "has-success"
                                }
                                invalid={
                                    this.state.validate.nameState ===
                                    "has-danger"
                                }
                                value={this.state.name}
                                onChange={e => {
                                    this.changeName(e);
                                    this.validateName(e);
                                }}
                                type="text"
                                name="name"
                                id="theName"
                                placeholder="Your name..."
                            />
                            <FormFeedback>Review name typed.</FormFeedback>
                        </FormGroup>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0 col-xs-12">
                            <Label for="Email" className="mr-sm-2">
                                Email
                            </Label>
                            <Input
                                valid={
                                    this.state.validate.emailState ===
                                    "has-success"
                                }
                                invalid={
                                    this.state.validate.emailState ===
                                    "has-danger"
                                }
                                value={this.state.email}
                                onChange={e => {
                                    this.changeEmail(e);
                                    this.validateEmail(e);
                                }}
                                type="email"
                                name="email"
                                id="Email"
                                placeholder="user@domain.com"
                            />
                            <FormFeedback>Review email typed.</FormFeedback>
                        </FormGroup>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="Password" className="mr-sm-2">
                                Password
                            </Label>
                            <Input
                                valid={
                                    this.state.validate.passState ===
                                    "has-success"
                                }
                                invalid={
                                    this.state.validate.passState ===
                                    "has-danger"
                                }
                                value={this.state.pass}
                                onChange={e => {
                                    this.changePass(e);
                                    this.validatePass(e);
                                }}
                                type="password"
                                name="password"
                                id="Password"
                                placeholder="your pass"
                            />
                            <FormFeedback>Review Password typed.</FormFeedback>
                        </FormGroup>{" "}
                        <div className="mt-2">
                            {" "}
                            <Button
                                block
                                color="info"
                                onClick={e => this.onSubmit(e)}
                                disabled={disabled}
                            >
                                Register!
                            </Button>
                        </div>
                    </Form>
                );
            } else {
                return (
                    <Alert color="primary">
                        <strong>{this.props.register.name}</strong>, you can now
                        login with your credentials.
                    </Alert>
                );
            }
        } else {
            return null;
        }
    }
}
