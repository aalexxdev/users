/*import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers/allReducers";
import thunk from "redux-thunk";
import logger from "redux-logger";
export default function configureStore() {
    return createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
            window.__REDUX_DEVTOOLS_EXTENSION__(),
        compose(applyMiddleware(thunk, logger))
    );
}*/

import { createStore, applyMiddleware, compose } from "redux";

import thunk from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import rootReducer from "../reducers/allReducers";
import logger from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import hardSet from "redux-persist/lib/stateReconciler/hardSet";
export const history = createHistory();
const persistConfig = {
    key: "root",
    storage: storage,
    stateReconciler: hardSet
};
const enhancers = [];
const middleware = [thunk];

if (process.env.NODE_ENV === "development") {
    const devToolsExtension = window.devToolsExtension;

    if (typeof devToolsExtension === "function") {
        enhancers.push(devToolsExtension());
    }
}

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    applyMiddleware(logger),
    ...enhancers
);

export default () => {
    let store = createStore(
        persistReducer(persistConfig, rootReducer),
        composedEnhancers
    );

    let persistor = persistStore(store);
    return { store, persistor };
};
